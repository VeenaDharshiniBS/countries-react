Simple Countries diplaying project with

- searching a country by name
- filtering countries by region (Asia/ Europe/ Africa/ America/ Oceania)
- pop-up box displaying extra details of a particular country

Data fetched from [https://restcountries.com/v3.1/all] api
Deloyed site [https://countries-react-app-veena.netlify.app/]
