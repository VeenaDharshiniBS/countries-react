import React, { Component } from 'react';
import './filter.css';

class Filter extends Component {
    state = { 
        transformStyle: false,
     }

    handleFilterClick() {
        this.setState({ transformStyle: !this.state.transformStyle });
    }

    render() {
        const {onFilter} = this.props;

        return (
            <div className="filter-country">
                <button onClick={() => {this.handleFilterClick()}} className="click-filter">Filter by Region <span className="material-icons expand-icon">expand_more</span></button>
                <div className="region-list" style={{transform : this.state.transformStyle==true ? "scaleY(1)" : "scaleY(0)"}}>
                    <button onClick={()=>{onFilter("Africa");
                                        this.handleFilterClick()}} className="region">Africa</button>
                    <button onClick={()=>{onFilter("Americas");
                                        this.handleFilterClick();}} className="region">America</button>
                    <button onClick={()=>{onFilter("Asia");
                                        this.handleFilterClick();}} className="region">Asia</button>
                    <button onClick={()=>{onFilter("Europe");
                                        this.handleFilterClick();}} className="region">Europe</button>
                    <button onClick={()=>{onFilter("Oceania");
                                        this.handleFilterClick();}} className="region">Oceania</button>
                </div>
            </div>
            
            );
        }
    }
 
export default Filter;
