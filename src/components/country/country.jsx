import React, { Component } from 'react';
import CountryModal from '../countryModal/countryModal';
import "./country.css";

class Country extends Component {   

    state = {
        visible: false,
    };

    constructor(props)
    {
        super(props);
        this.handleOpen = this.handleOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    handleOpen()
    {
        this.setState({visible: true});
    }

    handleClose()
    {
        this.setState({visible: false});
    }

    render() { 
        let {country} = this.props;

        return (
            <div className='country'>
                <div onClick={()=>this.handleOpen()} className='country-flag'>
                    <img src={country.flags.svg}></img>
                </div>
                <div onClick={()=>this.handleOpen()} className='country-details'>
                    <div className='country-name'>{country.name.common}</div>
                    <div><span className="heading">Population: </span>{country.population}</div>
                    <div><span className="heading">Region: </span>{country.region}</div>
                    <div><span className="heading">Capital: </span>{country.capital}</div>
                </div>
                {(this.state.visible && <CountryModal onClose={this.handleClose} country = {country}/>)}
            </div>
        );
    }
}
 
export default Country;