import React, { Component } from 'react';
import './search.css';

class Search extends Component {
    render() {
        const {onSearch} = this.props;
        return (
            <div className="search-country">
                <span className="material-icons">search</span>
                <input type="search" id="search" placeholder="Search for a country..." onChange={onSearch}></input>
            </div>
        );
    }
}
 
export default Search;