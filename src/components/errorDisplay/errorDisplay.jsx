import React, { Component } from 'react';
import './errorDisplay.css';

class ErrorDisplay extends Component {

    render() { 
        return (
        <div className="error-display">
            <h3>Something went wrong!</h3>
        </div>);
    }
}
 
export default ErrorDisplay;