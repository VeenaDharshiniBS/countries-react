import React, { Component } from 'react';
import './countryModal.css';

class CountryModal extends Component {

    render() {
        let {country, onClose} = this.props;

        function getNativeName(nativeName) {
            if (typeof nativeName != 'object')
              return " ";
          
            let ans = [];
          
            function helper(obj) {
              for (let index = 0; index < Object.entries(obj).length; index++) {
                let value = Object.entries(obj)[index][1];
                let key = Object.entries(obj)[index][0];
                if (typeof value == 'object')
                  helper(value);
                else {
                  if (key == "common")
                    ans.push(value);
                }
          
              }
            }
            helper(nativeName);
            return ans;
          }

          function getCurrencyName(country) {
            if(typeof country.currencies != 'object')
              return " ";
            return Object.values(country.currencies)[0].name;
          }
          
          function getLanguages(country)
          {
            if(typeof country.languages != 'object')
              return " ";
            return Object.values(country.languages);
          }

          function getBorderCountries(country){
            if(country.hasOwnProperty("borders")==false)
              return " ";
            let id = 0;
            let res = country.borders;
            res = res.map((border)=><div key={id++}>{border}</div>)
            return res;
          }

          window.addEventListener('click', (event) => {
            if (event.target.className == "modal-container") {
              onClose();
            }
          });

          document.addEventListener('keydown', function(event) {
            const key = event.key;
            if (key === "Escape") {
              onClose();
            }
        });

        return (
        <div className="modal-container">
            <div className="modal-content">
                <div className="modal-flag">
                    <img src={country.flags.svg}></img>
                </div>
                <div className="modal-details">
                    <div className="modal-header">{country.name.common}</div>
                    <div onClick={()=>onClose()} className="close">&times;</div>
                    <div className="modal-details-1">
                        <div><span className="heading">Native Name: </span>{getNativeName(country.name.nativeName)}</div>
                        <div><span className="heading">Region: </span>{country.region}</div>
                        <div><span className="heading">Sub Region: </span>{country.subregion}</div>
                        <div><span className="heading">Capital: </span>{country.capital}</div>
                        <div><span className="heading">Population: </span>{country.population}</div>
                    </div>
                    <div className="modal-details-2">
                        <div><span className="heading">Top Level Domain: </span>{country.tld}</div>
                        <div><span className="heading">Currencies: </span>{getCurrencyName(country)}</div>
                        <div><span className="heading">Languages: </span>{getLanguages(country)}</div>
                    </div>
                    <div className="modal-border">
                      <span className="heading">Border Countries</span>
                      <div className="modal-border-countries">
                        {getBorderCountries(country)}
                      </div>

                    </div>
                </div>
            </div>
        </div>);
    }
}
 
export default CountryModal;