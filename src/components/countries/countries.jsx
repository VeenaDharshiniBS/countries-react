import React, { Component } from 'react';
import "./countries.css";
import Country from "../country/country";

class Countries extends Component {

    render() {
        const {countries} = this.props;
        return (
            countries &&
            countries.length > 0 &&
            countries.map((country,index)=>(
                    <Country country = {country} key={index+1} />
            ))
        );
    }
}
 
export default Countries;