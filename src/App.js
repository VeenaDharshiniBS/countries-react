import React, { Component } from "react";
import "./App.css";
import Countries from "./components/countries/countries";
import Header from "./components/header";
import Search from "./components/search/search";
import Filter from "./components/filter/filter";
import Loader from "./components/loader/loader";
import ErrorDisplay from "./components/errorDisplay/errorDisplay";
import axios from "axios";

class App extends Component {
  state = {
    countries: [],
    searchWord: "",
    searchedCountries: [],
    selectRegion: "",
    loading: true,
    errorDisplay: false,
  };

  componentDidMount() {
    axios
      .get("https://restcountries.com/v3.1/all")
      .then((res) => {
        this.setState({ countries: res.data });
        this.setState({ loading: false });
      })
      .catch((err) => {
        this.setState({ errorDisplay: true });
      });
  }

  constructor(props) {
    super(props);
    this.handleSearch = this.handleSearch.bind(this);
    this.handleFilter = this.handleFilter.bind(this);
  }

  handleSearch = (event) => {
    let searchWord = event.target.value.toLowerCase();
    this.setState({ searchWord: searchWord });
    let filterCountries = this.state.countries.filter((country) =>
      country.name.common.toLowerCase().includes(this.state.searchWord)
    );
    this.setState({ searchedCountries: filterCountries });
  };

  handleFilter = (value) => {
    let selectRegion = value;
    this.setState({ selectRegion: selectRegion });
    let filterCountries = this.state.countries.filter(
      (country) => country.region == value
    );
    this.setState({ searchedCountries: filterCountries });
  };

  render() {
    return (
      <React.Fragment>
        <Header></Header>
        {this.state.loading && !this.state.errorDisplay && <Loader></Loader>}
        {this.state.errorDisplay && <ErrorDisplay></ErrorDisplay>}
        {!this.state.errorDisplay && (
          <div className="search-bar">
            <Search onSearch={this.handleSearch} />
            <Filter onFilter={this.handleFilter} />
          </div>
        )}
        {!this.state.errorDisplay && (
          <div className="countries">
            <Countries
              countries={
                this.state.searchedCountries.length != 0
                  ? this.state.searchedCountries
                  : this.state.countries
              }
            />
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default App;
